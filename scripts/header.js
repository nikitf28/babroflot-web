function getCoords(elem) { // кроме IE8-
  var box = $(elem)[0].getBoundingClientRect();

  return {
    top: box.top + pageYOffset,
    left: box.left + pageXOffset,
    bottom: box.bottom + pageYOffset,
    right: box.right + pageXOffset

  };

}

function resizeSearch() {
  //console.log("resize search");
  let posLogo = getCoords("header .headerLogo").right + 20;
  let posSearch = getCoords("header #searchBtn").left;
  let width = posSearch - posLogo;
  //alert(top);
  $("header #searchBox").css("width", width);
}

function resizeCover(){
  //console.log("resize cover");
  let widthBody = $("body").css("width");
  let heightBody = $("body").css("height");
  let top = $("header").css("height");
  let left = -parseInt($("header .menuBox").css("left"));
  $("header .cover").css("height", heightBody);
  $("header .cover").css("width", widthBody);
  $("header .cover").css("top", top);
  $("header .menuBox").css("width", widthBody);
  $("header .menuBox").offset({left: 0});
  console.log($("header .menuBox").offset().top);
  console.log($("header .menuBox").offset().left);

}



setInterval(function() {
  $("header .menuBox").offset({left: 0});
    if($("header #searchBtn").is(":checked")) {
        $("header .cover").css("display", "block");
    }
    else {
        $("header .cover").css("display", "none");
    }

}, 100);

$(document).mouseup(function (e) {
    var bla = 1;
    var container = $("header .chooseBox");
    var container2 = $("header .menuBox");
      if (container.has(e.target).length === 0 &&
      container2.has(e.target).length === 0 &&
      !container.is(e.target) && !container2.is(e.target)
      ){
        $("header input[type=checkbox]").attr("checked", false);
        $("header .leftMenuCheck ~ .menuBox").css("display", "none");
      }

});
$( document ).ready(function(){
  let ch = 0;
  $( "header .leftMenuCheck" ).change(function() {
    if ( $(this).is(':checked') ) {
        $(".header leftMenuCheck:checked ~ .menuBox").css("opacity", "0");

        setTimeout(function(){$("header .leftMenuCheck:checked ~ .menuBox").css("display", "block");}, 500);
        //$(".menuBack").css("overflow", "visible");
        let top1 = $("header").height();
        let top2 = $("header .leftMenuCheck:checked ~.menuBox").height();

        $("header .menuBack").css("top", top1);
        $("header .menuBack").animate({ "height": top2}, 500);
        $("header .menuBack").css("height", top2);
        ch = 1;
        setTimeout(function() {
            $("header .leftMenuCheck:checked ~ .menuBox").css("opacity", "1");
        }, 500);

    }
    $(document).mouseup(function (e) {
      var container = $("header .menuBox");
      var container2 = $("header .leftMenuCheck");
        if(!container.is(e.target) &&
          container.has(e.target).length === 0 &&
          !container2.is(e.target) && ch == 1){
            ch = 0;
            $("header .menuBack").animate({  "height": 0}, 500);
            //$(".menuBack").css("height", 0);
        }
    });

  }).change();
});
// установим обработчик события resize
$(window).resize(function(){
  resizeSearch();
  resizeCover();
});

window.onload = function() {
  resizeSearch();
  resizeCover();
};

// вызовем событие resize
//$(window).resize();
