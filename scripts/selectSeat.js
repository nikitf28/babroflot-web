var selected = 0;

$( document ).ready(function(){

  $('.seat input').click(function() {
    console.log(selected);
      if ($(this).is(":checked")){
        if (parseInt(selected) < 3 &&
            $(this).parent().css('background-color') != 'rgb(170, 170, 170)'){
          selected = parseInt(selected) + 1;
          $(".yourSeats span").val(selected);
          $(this).parent().css('background-color', '#133AAC');
        }
        else {
          $(this).attr('checked', false);
        }
      }
      else{
        selected = parseInt(selected) - 1;
        $(this).parent().css('background', '');
      }
      $(".yourSeats span").text(selected);
  });
});
